<?php

namespace cpl\citation_manager;


/*
initially, was going to write some conditional statement that would only run create_the_citation_role if the role didn't already exist (in the database),
but what if I add or remove capabilities after the role already exists ?

Thus, the role_exists function is there but it's not used... create_the_citation_role is only going to run anyways when the plugin is activated;
so fine for now

function role_exists( $role ) {
	if ( ! empty( $role ) ) {
		return wp_roles()->is_role( $role );
	}

	return false;
}
*/

/* create the role (citation_manager) and grant several capabilities to the role

capabilities include managing posts of the cpl_citation post type and taxonomies associated with that post type
*/


function create_the_citation_role_and_add_caps() {
	add_role(
		'citation_manager',
		'Citation Manager',
		array(
			'edit_cpl_citation'              => true,
			'read_cpl_citation'              => true,
			'delete_cpl_citation'            => true,
			'edit_cpl_citations'             => true,
			'edit_others_cpl_citations'      => true,
			'publish_cpl_citations'          => true,
			'read_private_cpl_citations'     => true,
			'delete_cpl_citations'           => true,
			'delete_private_cpl_citations'   => true,
			'delete_published_cpl_citations' => true,
			'delete_others_cpl_citations'    => true,
			'edit_private_cpl_citations'     => true,
			'edit_published_cpl_citations'   => true,
			'edit_cpl_citations'             => true,
			'manage_citation_authors'        => true,
			'edit_citation_authors'          => true,
			'delete_citation_authors'        => true,
			'assign_citation_authors'        => true,
			'manage_citation_locations'      => true,
			'edit_citation_locations'        => true,
			'delete_citation_locations'      => true,
			'assign_citation_locations'      => true,
			'manage_citation_types'          => true,
			'edit_citation_types'            => true,
			'delete_citation_types'          => true,
			'assign_citation_types'          => true,
		)
	);
}
register_activation_hook( __FILE__, 'create_the_citation_role_and_add_caps' );


