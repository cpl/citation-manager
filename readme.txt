=== Citation-Manager ===
Contributors:      Will Skora and the CPL team
Tested up to:      6.7.x
Stable tag:        0.8.3
Requires PHP:      7.2
License:           GPL-2.0-or-later
License URI:       https://www.gnu.org/licenses/gpl-2.0.html
Plugin URI:		     https://gitlab.com/cpl/citation-manager

== Description ==

A theme-agnostic WP plugin for Creating and managing Citations (custom post type) and taxonomies specifically for cpl.org's Local African American History Directory.

== Notes ==

Not yet stable, no support implied.

== Changelog ==

0.8.3
- Additional validation for input fields

0.8.2
- correctly save and update term_meta for citation_location taxonomy

0.8.0
- register term_meta for the citation_location taxonomy

0.6.1

- Fix function name

0.6.0
- Add capabilities for taxonomies; create role and assign capabilities to edit those taxonomies and the custom post type, to that role
