<?php

namespace cpl\citation_manager;
/**
 * Registers a custom post type 'cpl_citation'.
 *
 * @since 1.0.0
 *
 * @return void
 */
function register_cpl_citation() : void {
	$labels       = array(
		'name'                  => _x( 'Citations', 'Post Type General Name', 'citation_manager' ),
		'singular_name'         => _x( 'Citation', 'Post Type Singular Name', 'citation_manager' ),
		'menu_name'             => __( 'Citations', 'citation_manager' ),
		'name_admin_bar'        => __( 'Citations', 'citation_manager' ),
		'archives'              => __( 'Citations Archives', 'citation_manager' ),
		'attributes'            => __( 'Citations Attributes', 'citation_manager' ),
		'parent_item_colon'     => __( 'Parent Citation:', 'citation_manager' ),
		'all_items'             => __( 'All Citations', 'citation_manager' ),
		'add_new_item'          => __( 'Add New Citation', 'citation_manager' ),
		'add_new'               => __( 'Add New', 'citation_manager' ),
		'new_item'              => __( 'New Citation', 'citation_manager' ),
		'edit_item'             => __( 'Edit Citation', 'citation_manager' ),
		'update_item'           => __( 'Update Citation', 'citation_manager' ),
		'view_item'             => __( 'View Citation', 'citation_manager' ),
		'view_items'            => __( 'View Citations', 'citation_manager' ),
		'search_items'          => __( 'Search Citations', 'citation_manager' ),
		'not_found'             => __( 'Citation Not Found', 'citation_manager' ),
		'not_found_in_trash'    => __( 'Citation Not Found in Trash', 'citation_manager' ),
		'featured_image'        => __( 'Featured Image', 'citation_manager' ),
		'set_featured_image'    => __( 'Set Featured Image', 'citation_manager' ),
		'remove_featured_image' => __( 'Remove Featured Image', 'citation_manager' ),
		'use_featured_image'    => __( 'Use as Featured Image', 'citation_manager' ),
		'insert_into_item'      => __( 'Insert into Citation', 'citation_manager' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Citation', 'citation_manager' ),
		'items_list'            => __( 'Citations List', 'citation_manager' ),
		'items_list_navigation' => __( 'Citations List Navigation', 'citation_manager' ),
		'filter_items_list'     => __( 'Filter Citations List', 'citation_manager' ),
	);
	$labels       = apply_filters( 'cpl_citation-labels', $labels );
	$capabilities = array(
		'edit_post'              => 'edit_cpl_citation',
		'read_post'              => 'read_cpl_citation',
		'delete_post'            => 'delete_cpl_citation',
		'edit_posts'             => 'edit_cpl_citations',
		'edit_others_posts'      => 'edit_others_cpl_citations',
		'publish_posts'          => 'publish_cpl_citations',
		'read_private_posts'     => 'read_private_cpl_citations',
		'delete_posts'           => 'delete_cpl_citations',
		'delete_private_posts'   => 'delete_private_cpl_citations',
		'delete_published_posts' => 'delete_published_cpl_citations',
		'delete_others_posts'    => 'delete_others_cpl_citations',
		'edit_private_posts'     => 'edit_private_cpl_citations',
		'edit_published_posts'   => 'edit_published_cpl_citations',
		'create_posts'           => 'edit_cpl_citations',
	);
	$args         = array(
		'label'               => __( 'Citation', 'citation_manager' ),
		'description'         => __( 'Citation ', 'citation_manager' ),
		'labels'              => $labels,
		'supports'            => array(
			'title',
			'editor',
			'author',
			'custom-fields',
			'page-attributes',
		),
		'taxonomies'          => array( 'citation_author', 'citation_location', 'citation_type' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-book',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'can_export'          => true,
		'map_meta_cap'        => true,
		'capabilities'        => $capabilities,
		'capability_type'     => array( 'cpl_citation', 'cpl_citations' ),
		'show_in_rest'        => true,
	);
	$args         = apply_filters( 'cpl_citation-args', $args );

	register_post_type( 'cpl_citation', $args );
}


// determine which columns at edit.php?post_type=citation display
// seems a little hacky in that I just generated
// t3h_agenda_date; you can also rename column_name as well
function modify_cpl_citation_post_admin_columns( $columns ) {
	unset( $columns['author'] );
	unset( $columns['date'] );
	unset( $columns['last-modified'] );
	return array_merge(
		$columns,
	);
}
add_filter( 'manage_cpl_citation_posts_columns', __NAMESPACE__ . '\modify_cpl_citation_post_admin_columns' );
