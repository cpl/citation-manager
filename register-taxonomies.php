<?php

namespace cpl\citation_manager;
/**
 * Registers the taxonomies
 *
 * @since 1.0.0
 *
 * @return void
 */
function register_taxonomy_citation_author() {
	$args = array(
		'labels'             => array(
			'menu_name'                  => __( 'Citation Authors', 'citation_manager' ),
			'all_items'                  => __( 'All Citation Authors', 'citation_manager' ),
			'edit_item'                  => __( 'Edit Citation Author', 'citation_manager' ),
			'view_item'                  => __( 'View Citation Author', 'citation_manager' ),
			'update_item'                => __( 'Update Citation Author', 'citation_manager' ),
			'add_new_item'               => __( 'Add new Citation Author', 'citation_manager' ),
			'new_item_name'              => __( 'New Citation Author Name', 'citation_manager' ),
			'parent_item'                => __( 'Parent Citation Author', 'citation_manager' ),
			'parent_item_colon'          => __( 'Parent Citation Author:', 'citation_manager' ),
			'search_items'               => __( 'Search Citation Authors', 'citation_manager' ),
			'popular_items'              => __( 'Popular Citation Authors', 'citation_manager' ),
			'separate_items_with_commas' => __( 'Separate Citation Authors with commas', 'citation_manager' ),
			'add_or_remove_items'        => __( 'Add or remove Citation Authors', 'citation_manager' ),
			'choose_from_most_used'      => __( 'Choose most used Citation Authors', 'citation_manager' ),
			'not_found'                  => __( 'No Citation Authors found', 'citation_manager' ),
			'name'                       => _x( 'Citation Authors', 'taxonomy general name' ),
			'singular_name'              => _x( 'Citation Author', 'taxonomy general name' ),
		),
		'capabilities'       => array(
			'manage_terms' => 'manage_citation_authors',
			'edit_terms'   => 'edit_citation_authors',
			'delete_terms' => 'delete_citation_authors',
			'assign_terms' => 'assign_citation_authors',
		),

		'public'             => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => true,
		'show_tagcloud'      => false,
		'show_in_quick_edit' => true,
		'show_admin_column'  => true,
		'show_in_rest'       => true,
		'hierarchical'       => false,
		'query_var'          => true,
		'rewrite'            => true,
	);
	register_taxonomy( 'citation_author', array( 'cpl_citation' ), $args );
}
add_action( 'init', __NAMESPACE__ . '\register_taxonomy_citation_author' );


function register_taxonomy_citation_location() {
	$args = array(
		'labels'             => array(
			'menu_name'                  => __( 'Citation Locations', 'citation_manager' ),
			'all_items'                  => __( 'All Citation Locations', 'citation_manager' ),
			'edit_item'                  => __( 'Edit Citation Location', 'citation_manager' ),
			'view_item'                  => __( 'View Citation Location', 'citation_manager' ),
			'update_item'                => __( 'Update Citation Location', 'citation_manager' ),
			'add_new_item'               => __( 'Add new Citation Location', 'citation_manager' ),
			'new_item_name'              => __( 'New Citation Location Name', 'citation_manager' ),
			'parent_item'                => __( 'Parent Citation Location', 'citation_manager' ),
			'parent_item_colon'          => __( 'Parent Citation Location:', 'citation_manager' ),
			'search_items'               => __( 'Search Citation Locations', 'citation_manager' ),
			'popular_items'              => __( 'Popular Citation Locations', 'citation_manager' ),
			'separate_items_with_commas' => __( 'Separate Citation Locations with commas', 'citation_manager' ),
			'add_or_remove_items'        => __( 'Add or remove Citation Locations', 'citation_manager' ),
			'choose_from_most_used'      => __( 'Choose most used Citation Locations', 'citation_manager' ),
			'not_found'                  => __( 'No Citation Locations found', 'citation_manager' ),
			'name'                       => _x( 'Citation Locations', 'taxonomy general name' ),
			'singular_name'              => _x( 'Citation Location', 'taxonomy general name' ),
		),
		'capabilities'       => array(
			'manage_terms' => 'manage_citation_locations',
			'edit_terms'   => 'edit_citation_locations',
			'delete_terms' => 'delete_citation_locations',
			'assign_terms' => 'assign_citation_locations',
		),

		'public'             => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => true,
		'show_tagcloud'      => false,
		'show_in_quick_edit' => true,
		'show_admin_column'  => true,
		'show_in_rest'       => true,
		'hierarchical'       => false,
		'query_var'          => true,
		'rewrite'            => true,
	);
	register_taxonomy( 'citation_location', array( 'cpl_citation' ), $args );
}
add_action( 'init', __NAMESPACE__ . '\register_taxonomy_citation_location' );

function register_taxonomy_citation_type() {
	$args = array(
		'labels'             => array(
			'menu_name'                  => __( 'Citation Types', 'citation_manager' ),
			'all_items'                  => __( 'All Citation Types', 'citation_manager' ),
			'edit_item'                  => __( 'Edit Citation Type', 'citation_manager' ),
			'view_item'                  => __( 'View Citation Type', 'citation_manager' ),
			'update_item'                => __( 'Update Citation Type', 'citation_manager' ),
			'add_new_item'               => __( 'Add new Citation Type', 'citation_manager' ),
			'new_item_name'              => __( 'New Citation Type Name', 'citation_manager' ),
			'parent_item'                => __( 'Parent Citation Type', 'citation_manager' ),
			'parent_item_colon'          => __( 'Parent Citation Type:', 'citation_manager' ),
			'search_items'               => __( 'Search Citation Types', 'citation_manager' ),
			'popular_items'              => __( 'Popular Citation Types', 'citation_manager' ),
			'separate_items_with_commas' => __( 'Separate Citation Types with commas', 'citation_manager' ),
			'add_or_remove_items'        => __( 'Add or remove Citation Types', 'citation_manager' ),
			'choose_from_most_used'      => __( 'Choose most used Citation Types', 'citation_manager' ),
			'not_found'                  => __( 'No Citation Types found', 'citation_manager' ),
			'name'                       => _x( 'Citation Types', 'taxonomy general name' ),
			'singular_name'              => _x( 'Citation Type', 'taxonomy general name' ),
		),
		'capabilities'       => array(
			'manage_terms' => 'manage_citation_types',
			'edit_terms'   => 'edit_citation_types',
			'delete_terms' => 'delete_citation_types',
			'assign_terms' => 'assign_citation_types',
		),
		'public'             => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => true,
		'show_tagcloud'      => false,
		'show_in_quick_edit' => true,
		'show_admin_column'  => true,
		'show_in_rest'       => true,
		'hierarchical'       => false,
		'query_var'          => true,
		'rewrite'            => true,
	);
	register_taxonomy( 'citation_type', array( 'cpl_citation' ), $args );
}
add_action( 'init', __NAMESPACE__ . '\register_taxonomy_citation_type' );
