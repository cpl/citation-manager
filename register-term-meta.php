<?php

namespace cpl\citation_manager;

// creates the interface for creating, editing term meta fieds on /wp-admin/edit-tags.php?taxonomy=citation_location

function citation_location_term_array_as_function() {

	return array(
		'citation_location_address'   => esc_html__( 'Location Physical Address', 'citation_manager' ),
		'citation_location_website'   => esc_html__( 'Location Website (include https://)', 'citation_manager' ),
		'citation_location_contact'   => esc_html__( 'Location Contact Person', 'citation_manager' ),
		'citation_location_email'     => esc_html__( 'Email', 'citation_manager' ),
		'citation_location_telephone' => esc_html__( 'Phone', 'citation_manager' ),

	);
}

// create the meta fields input on the taxonomy page;
// note: do not wrap it within a form element, b/c remember you're injecting this HTML into the existing form element;

function add_citation_location_fields( $taxonomy ) {
	$citation_location_term_array = citation_location_term_array_as_function();

	foreach ( $citation_location_term_array as $each_option => $value ) {
		?>
   <div class="form-field term-group">
	   <label for="<?php printf( esc_html__( '%s-metadata', 'citation_manager' ), esc_attr( $each_option ) ); ?>">
			<?php printf( esc_html__( '%s ', 'citation_manager' ), esc_html( $value ) ); ?>
	</label>
	 <input type="text"
			 name="<?php printf( esc_html__( '%s_metadata', 'citation_manager' ), esc_attr( $each_option ) ); ?>"
			 id="<?php printf( esc_html__( '%s-metadata', 'citation_manager' ), esc_attr( $each_option ) ); ?>"
			value=""
		/>


	</div>
			<?php
	}
}

	// these hook names are created by wordpress, based on the name of our taxonomy (citation_location)
	// use the same callback function (save_our_term_meta ) to both edit and
	// yes, it's weird that one of them is prefixed created_ and the other is prefixed edit_
	// edited_{taxonomy} (as a hook) does exist; but the edit_ works (for now..)
	add_action( 'citation_location_add_form_fields', __NAMESPACE__ . '\add_citation_location_fields' );
	add_action( 'citation_location_edit_form_fields', __NAMESPACE__ . '\edit_feature_group_field' );
	add_action( 'created_citation_location', __NAMESPACE__ . '\save_our_term_meta' );
	add_action( 'edit_citation_location', __NAMESPACE__ . '\save_our_term_meta' );

function edit_feature_group_field( $term ) {

	$citation_location_term_array = citation_location_term_array_as_function();
	?>
	   <th scope="row" valign="top" colspan="2">
		<h3><?php esc_html_e( 'Additional Options', 'citation-manager' ); ?></h3>
	</th>
	<?php
	// get current group
	foreach ( $citation_location_term_array as $each_option => $value ) {
		$term_key = sprintf( '%s_metadata', $each_option );
		$metadata = get_term_meta( $term->term_id, $term_key, true );
		?>

<tr class="form-field">
 <th scope="row">
 <label for="<?php printf( esc_html__( '%s-metadata', 'citation_manager' ), esc_attr( $each_option )); ?>">
			<?php printf( esc_html__( '%s ', 'citation_manager' ), esc_html( $value ) ); ?>
	</label>
 </th>

	<td>
		<input type="text"
				name="<?php printf( esc_html__( '%s_metadata', 'citation_manager' ), esc_attr( $each_option ) ); ?>"
				id="<?php printf( esc_html__( '%s-metadata', 'citation_manager' ), esc_attr( $each_option ) ); ?>"
				value="<?php echo ( ! empty( $metadata ) ) ? esc_attr( $metadata ) : ''; ?>"
			/>

		</td>
	</tr>
			<?php
	}
}

function save_our_term_meta( $term_id ) {
	$citation_location_term_array = citation_location_term_array_as_function();

	foreach ( $citation_location_term_array as $each_option => $value ) {
		// creating the key dynamically?
		// if field (from the form is not null); update it and save it
		// should I sanitize it anyways?
		$term_key = sprintf( '%s_metadata', $each_option );
		if ( isset( $_POST [ $term_key ] ) ) {
			update_term_meta( $term_id, esc_attr( $term_key ), $_POST[ $term_key ] );
		}
	}
}
