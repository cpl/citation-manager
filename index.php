<?php

namespace cpl\citation_manager;

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://skorasaur.us
 * @since             1.0.1
 * @package           citations
 *
 * @wordpress-plugin
 * Plugin Name:       CPL Citation Manager
 * Plugin URI:        https://gitlab.com/cpl/citation-manager
 * Description:       A theme-agnostic WP plugin for Managing Citations in the AFR-american archive
 *
 * Requires PHP:      7.2+
 * Version:           0.8.3
 * Author:            Will Skora
 * Author URI:        https://skorasaur.us
 * Update URI:        https://gitlab.com/cpl/citation-manager
 * License:           GPL-3.0+
 * License URI:       https://choosealicense.com/licenses/gpl-3.0/
 * Text Domain:       citation_manager
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// not sure if this a standard?
define( 'CPL_CIT_VERSION', '0.8.2' );
define( 'CPL_CIT_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );


require_once( CPL_CIT_PLUGIN_DIR . '/register-post-type.php' );
require_once( CPL_CIT_PLUGIN_DIR . '/register-taxonomies.php' );
require_once( CPL_CIT_PLUGIN_DIR . '/roles-capabilities.php' );
require_once( CPL_CIT_PLUGIN_DIR . '/register-term-meta.php' );


add_action( 'init', __NAMESPACE__ . '\register_cpl_citation', 0 );

// https://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation
// This is to flush rewrites so if I decide to change the path of the CPT, it will auotmatically
// do it on plugin activation
function my_rewrite_flush() {
	// First, we "add" the custom post type via the above written function.
	// Note: "add" is written with quotes, as CPTs don't get added to the DB,
	// They are only referenced in the post_type column with a post entry,
	// when you add a post of this CPT.
	register_cpl_citation();

	// ATTENTION: This is *only* done during plugin activation hook in this example!
	// You should *NEVER EVER* do this on every page load!!
	\flush_rewrite_rules();
}
register_activation_hook( __FILE__, __NAMESPACE__ . '\my_rewrite_flush' );
